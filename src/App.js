
import React, { useEffect, useState } from "react";
import './App.css'
import { saveAs } from 'file-saver';

const App = () => {

    const strokeWeight = document.querySelector('.stroke-weight');
    const colorPicker = document.querySelector('.color-picker');
    
    const [isDrawing, setIsDrawing] = useState(false)
    const [context, setContext] = useState({})
    const [canvas, setCanvas] = useState('')

    useEffect(() => {
        const getCanvas = document.getElementById("canvas");
        setCanvas(getCanvas)
        const getContext = getCanvas.getContext("2d");
        setContext(getContext)
    }, [])

    const setLineWidth = () => {
        context.lineWidth = strokeWeight.value;
        return context.lineWidth
    } 

    const setColor = () => {
        context.strokeStyle = colorPicker.value;
        return context.strokeStyle
    }

    const handleMouseMove = ({clientX: x, clientY: y}) => {
        if (!isDrawing) return;
        context.lineWidth = setLineWidth
        context.lineCap = "round";
        context.strokeStyle = setColor
        
        context.lineTo(x, y);
        context.stroke();
        context.beginPath();
        context.moveTo(x, y);
    }

    const handleMouseDown = (event) => {
        setIsDrawing(true)
        
        handleMouseMove(event)
    }

    const handleMouseUp = () => {
        setIsDrawing(false)
        context.beginPath();
    }

    const clearCanvas = () => {
        context.clearRect(0, 0, canvas.width, canvas.height);
    }

    const save = () => {
        const canvas2 = document.getElementById("canvas");
        canvas2.toBlob(function(blob) {
            saveAs(blob, "canvas.png");
        })
    }


    return (
        <div>
            <canvas id = 'canvas' 
                width = { window.innerWidth }
                height = { window.innerHeight }
                onMouseDown = {handleMouseDown}
                onMouseMove = {handleMouseMove}
                onMouseUp = {handleMouseUp}

                ></canvas>
            <main>
                <section className="colors">
                    <input type="text" className="color-picker" placeholder='Set Color' onChange = {setColor} />
                </section>
                <section className="thickness">
                    <input type="number" className="stroke-weight" placeholder='Set Width' min="1" onChange = {setLineWidth}/>
                </section>
                <button title='Clear' className="clear" onClick = {clearCanvas}>X</button><br/>
                <button title='Save' className="clear" id='save' onClick = {save}><img src='https://png.pngtree.com/element_our/20190528/ourlarge/pngtree-cartoon-download-icon-download-image_1163019.jpg' className='download'/></button>
            </main>
        </div>
    )

}

export default App
